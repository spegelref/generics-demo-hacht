package com.monator.demo.generics.hacht.model;

public abstract class BasicElasticSerializable implements ElasticSerializable {
    @Override
    public String getType() {
        return this.getClass().getSimpleName().toLowerCase();
    }
}
