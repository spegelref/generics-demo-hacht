package com.monator.demo.generics.hacht.internal;

import com.monator.demo.generics.hacht.model.ElasticSerializable;

public interface ObjectFactory<T extends ElasticSerializable> {
    T generate(long id);
}
