package com.monator.demo.generics.hacht.model;

public class Process extends BasicElasticSerializable {
    private long id;

    public Process(long id) {
        this.id = id;
    }

    @Override
    public String getObject() {
        return String.format("{\"process-id\": %d}", id);
    }
}
