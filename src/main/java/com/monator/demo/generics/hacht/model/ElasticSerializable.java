package com.monator.demo.generics.hacht.model;

public interface ElasticSerializable {
    String getType();
    String getObject();
}
