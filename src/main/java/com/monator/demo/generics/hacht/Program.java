package com.monator.demo.generics.hacht;

import com.monator.demo.generics.hacht.index.Indexer;
import com.monator.demo.generics.hacht.internal.ProcessObjectFactory;

public class Program {
    public static void main(String[] args) {
        Indexer indexer = new Indexer("localhost:9200", "jbpm");

        indexer.index(new ProcessObjectFactory());
    }
}
