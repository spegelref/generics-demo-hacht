package com.monator.demo.generics.hacht.internal;

import com.monator.demo.generics.hacht.model.Process;

public class ProcessObjectFactory implements ObjectFactory<Process> {
    @Override
    public Process generate(long id) {
        return new Process(id);
    }
}
