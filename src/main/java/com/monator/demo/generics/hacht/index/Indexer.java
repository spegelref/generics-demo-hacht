package com.monator.demo.generics.hacht.index;

import com.monator.demo.generics.hacht.internal.ObjectFactory;
import com.monator.demo.generics.hacht.model.ElasticSerializable;

import java.util.logging.Logger;

public class Indexer {
    private static final Logger LOGGER =
            Logger.getLogger(Indexer.class.getCanonicalName());

    private static final String URL_FORMAT = "https://%s/%s/%s/%d";

    private String host;
    private String index;

    public Indexer(String host, String index) {
        this.host = host;
        this.index = index;
    }

    public <T extends ElasticSerializable> void index(
            ObjectFactory<T> factory) {
        long id = 1L;

        T t = factory.generate(id);

        String type = t.getType();
        String obj = t.getObject();
        String url = String.format(URL_FORMAT, host, index, type, id);

        try {
            index(url, obj);
        } catch (Exception e) {
            LOGGER.warning("Got exception!");
        }
    }

    private void index(String url, String obj) throws Exception {
        LOGGER.info("Index " + obj + " to " + url);
    }
}
